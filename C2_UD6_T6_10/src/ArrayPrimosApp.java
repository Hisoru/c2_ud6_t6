import java.util.Scanner;

public class ArrayPrimosApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce el tama�o del array");
		int num = sc.nextInt();
		
		int array[] = rellenarArray(num);
		mostrarArray(array);

	}
	
	// M�todo que rellena el array con n�meros aleatorios primos
	public static int[] rellenarArray (int num) {
		
		int array[] = new int[num];
		int numeroAleatorio = 0;
		
		for (int i = 0; i < array.length; i++) {
			
			numeroAleatorio = (int) (Math.random() * ((9 - 0) + 1) + 0);
			
			if (comprobacionNumero(numeroAleatorio)) {
				
				array[i] = numeroAleatorio;
				
			}
			
		}
		
		return array;
		
	}
	
	// M�todo que muestra el valor i mayor n�mero del array
	public static void mostrarArray (int array[]) {
		
		int mayor = 0;
		
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Valor: " + array[i]);
			
			
			if (mayor < array[i]) {
				
				mayor = array[i];
				
			}
			
		}
		
		System.out.println(mayor);
		
	}
	
	// M�todo que comprueba si el n�mero generado aleatoriamente es primo o no
	public static boolean comprobacionNumero (int num) {
		
		boolean primo = true;
		
		for (int i = 2; i <= num / 2; i++) {
			
			if (num % i == 0) {
				
				primo = false;
				break;
				
			}
			
		}
		
		return primo;
		
	} 

}
