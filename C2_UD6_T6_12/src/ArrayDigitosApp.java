import java.util.Scanner;

public class ArrayDigitosApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce el tama�o del array");
		int num = sc.nextInt();
		
		System.out.println("Introduce un d�gito");
		int digito = sc.nextInt();
		
		int array[] = rellenarArray(num);
		
		mostrarArray(array);
		mostrarArray(arrayDigitos(array, num, digito));

	}
	
	// M�todo que rellena el array con n�meros aleatorios del 1 al 300
	public static int[] rellenarArray (int num) {
		
		int array[] = new int[num];
		
		for (int i = 0; i < array.length; i++) {
	
			array[i] = (int) (Math.random() * ((300 - 1) + 1) + 1);
			
		}
		
		return array;
		
	}
	
	// M�todo que muestra el valor de un array
	public static void mostrarArray (int array[]) {
		
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Valor: " + array[i]);
			
		}
		
		System.out.println();
		
	}
	
	// M�todo que recoge los valores del primer array y forma uno nuevo con los n�meros que terminan en el d�gito introducido por el usuario
	public static int[] arrayDigitos (int array[], int num, int digito) {
		
		int arrayDigitos[] = new int[num];
		
		int digitoNumero = 0;
		
		for (int i = 0; i < arrayDigitos.length; i++) {
			
			digitoNumero = array[i] - (array[i] / 10 * 10);
			
			if (digitoNumero == digito) {
				
				arrayDigitos[i] = array[i];
				
			}
			
		}
		
		return arrayDigitos;
		
	}

}
