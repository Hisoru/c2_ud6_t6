import java.util.Scanner;

import javax.swing.JOptionPane;

public class AreaFiguraApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("�De qu� figura quieres calcular el �rea?");
		String figura = sc.next();
		
		// 'Switch' que pide al usuario unos datos dependiendo de la figura que haya introducido
		switch (figura) {
		
			case "Circulo":
				System.out.println("Introduce un radio");
				double radio = sc.nextDouble();
				System.out.println(areaCirculo(radio));
				break;
				
			case "Triangulo":
				System.out.println("Introduce una base");
				double base = sc.nextDouble();
				System.out.println("Introduce una altura");
				double altura = sc.nextDouble();
				System.out.println(areaTriangulo(base, altura));
				break;
			
			case "Cuadrado":
				System.out.println("Introduce un lado");
				double lado = sc.nextDouble();
				System.out.println(areaCuadrado(lado));
				break;
			
			default:
				System.out.println("No has introducido una figura correcta");
		
		}

	}
	
	// M�todo que calcula el �rea de un c�rculo
	public static double areaCirculo (double radio) {
		
		double resultado = (radio * 2) * Math.PI;
		return resultado;
		
	}
	
	// M�todo que calcula el �rea de un tri�ngulo
	public static double areaTriangulo (double base, double altura) {
		
		double resultado = (base * altura) / 2;
		return resultado;
		
	}
	
	// M�todo que calcula el �rea de un cuadrado
	public static double areaCuadrado (double lado) {
		
		double resultado = lado * lado;
		return resultado;
		
	}

}
