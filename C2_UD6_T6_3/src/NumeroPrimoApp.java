import java.util.Scanner;

public class NumeroPrimoApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int num = sc.nextInt();
		
		System.out.println(comprobacionNumero(num));

	}
	
	// M�todo que comprueba si el n�mero introducido por el usuario es primo o no
	public static boolean comprobacionNumero (int num) {
		
		boolean primo = true;
		
		for (int i = 2; i <= num / 2; i++) {
			
			if (num % i == 0) {
				
				primo = false;
				break;
				
			}
			
		}
		
		return primo;
		
	}

}
