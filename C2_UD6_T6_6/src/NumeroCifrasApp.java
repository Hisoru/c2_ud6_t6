import java.util.Scanner;

public class NumeroCifrasApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero positivo");
		int num = sc.nextInt();
		
		// Condici�n 'if' que muestra las cifras del n�mero introducido por el usuario siempre que no sea un n�mero negativo
		if (num > 0) {
			
			System.out.println(cifras(num));
			
		} else {
			
			System.out.println("No has introducido un n�mero correcto");
			
		}

	}
	
	// M�todo que cuenta el n�mero de cifras que hay en el n�mero introducido por el usuario
	public static int cifras (int num) {
		
		int resultado = 0;
		
		while (num != 0) {
			
			num /= 10;
			resultado++;
			
		}
		
		return resultado;
		
	}

}
