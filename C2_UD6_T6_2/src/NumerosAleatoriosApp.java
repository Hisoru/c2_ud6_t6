import java.util.Scanner;

public class NumerosAleatoriosApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("�Cuantos n�meros aleatorios quieres generar?");
		int cantidad = sc.nextInt();
		
		System.out.println("Introduce un rango m�ximo");
		int max = sc.nextInt();
		
		System.out.println("Introduce un rango m�nimo");
		int min = sc.nextInt();
		
		// Bucle 'for' que genera la cantidad de n�meros aleatorios que ha introducido el usuario
		for (int i = 0 ; i < cantidad ; i++) {
			
			System.out.println(numerosAleatorios(max, min));
			
		}

	}
	
	// M�todo que genera n�meros aleatorios dado un m�ximo y un m�nimo
	public static int numerosAleatorios (int max, int min) {
		
		int numeroAleatorio = (int) (Math.random() * ((max - min) + 1) + min);
		return numeroAleatorio;
		
	}

}
