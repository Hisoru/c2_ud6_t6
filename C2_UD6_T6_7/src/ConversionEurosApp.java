import java.util.Scanner;

public class ConversionEurosApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce una cantidad de euros");
		double euros = sc.nextDouble();
		
		System.out.println("Introduce un tipo de moneda");
		String moneda = sc.next();
		
		conversion(euros, moneda);

	}
	
	// M�todo que convierte los euros introducidos por el usuario al tipo de moneda que ha introducido
	public static void conversion (double euros, String moneda) {
		
		double resultado = 0;
		
		switch (moneda) {
		
			case "Dolares":
				resultado = euros * 1.28611;
				System.out.println(resultado);
				break;
				
			case "Yenes":
				resultado = euros * 129.852;
				System.out.println(resultado);
				break;
			
			case "Libras":
				resultado = euros * 0.86;
				System.out.println(resultado);
				break;
			
			default:
				System.out.println("No has introducido una moneda correcta");
		
		}
		
	}

}
