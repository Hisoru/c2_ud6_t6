import java.util.Scanner;

public class DecimalBinarioApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int num = sc.nextInt();
		
		System.out.println(numeroBinario(num));

	}
	
	// M�todo que convierte el n�mero decimal introducido por el usuario a binario
	public static String numeroBinario (int num) {
		
		return Integer.toBinaryString(num);
		
	}

}
