import java.util.Scanner;

public class ArrayNumerosApp {

	public static void main(String[] args) {
		
		int array[] = rellenarArray(10);
		mostrarArray(array);

	}
	
	// M�todo que rellena el array con los n�meros que introduce el usuario
	public static int[] rellenarArray (int num) {
		
		Scanner sc = new Scanner(System.in);
		
		int array[] = new int[num];
		
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Introduce un n�mero");
			array[i] = sc.nextInt();
			
		}
		
		return array;
		
	}
	
	// M�todo que muestra el indice y valor del array
	public static void mostrarArray (int array[]) {
		
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Indice: " + i);
			System.out.println("Valor: " + array[i]);
			
		}
		
	}

}
