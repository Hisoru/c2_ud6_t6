import java.util.Scanner;

public class ArrayAleatorioApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce el tama�o del array");
		int num = sc.nextInt();
		
		int array[] = rellenarArray(num);
		mostrarArray(array);

	}
	
	// M�todo que rellena el array con n�meros aleatorios del 0 al 9
	public static int[] rellenarArray (int num) {
		
		int array[] = new int[num];
		
		for (int i = 0; i < array.length; i++) {
			
			array[i] = (int) (Math.random() * ((9 - 0) + 1) + 0);
			
		}
		
		return array;
		
	}
	
	// M�todo que muestra el valor y suma del array
	public static void mostrarArray (int array[]) {
		
		int suma = 0;
		
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Valor: " + array[i]);
			suma += array[i];
			
		}
		
		System.out.println(suma);
		
	}

}
