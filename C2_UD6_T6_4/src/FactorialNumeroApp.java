import java.util.Scanner;

public class FactorialNumeroApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int num = sc.nextInt();
		
		System.out.println(calculoFactorial(num));

	}
	
	// M�todo que calcula el factorial del n�mero introducido por el usuario
	public static int calculoFactorial (int num) {
		
		int resultado = 1;
		
		for (int i = 1; i <= num; i++) {
			
			resultado = resultado * i;
			
		}
		
		return resultado;
		
	}

}
