import java.util.Scanner;

public class ArrayDobleApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce el tama�o del array");
		int num = sc.nextInt();
		
		int primerArray[] = rellenarArray(num);
		int segundoArray[] = rellenarArray(num);
		
		mostrarArray(primerArray);
		mostrarArray(segundoArray);
		mostrarArray(arrayMultiplicado(num, primerArray, segundoArray));

	}
	
	// M�todo que rellena un array con n�meros aleatorios
	public static int[] rellenarArray (int num) {
		
		int array[] = new int[num];
		
		for (int i = 0; i < array.length; i++) {
	
			array[i] = (int) (Math.random() * ((9 - 0) + 1) + 0);
			
		}
		
		return array;
		
	}
	
	// M�todo que muestra el valor de un array
	public static void mostrarArray (int array[]) {
		
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Valor: " + array[i]);
			
		}
		
		System.out.println();
		
	}
	
	// M�todo que recoge los valores de los dos arrays anteriores y los multiplica entre si para formar uno nuevo
	public static int[] arrayMultiplicado (int num, int primerArray[], int segundoArray[]) {
		
		int array[] = new int[num];
		
		for (int i = 0; i < primerArray.length; i++) {
			
			array[i] = primerArray[i] * segundoArray[i];
			
		}
		
		return array;
		
	}

}
